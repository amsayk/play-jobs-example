import com.google.inject.AbstractModule

import com.google.inject.name.Names

import akka.stream.ActorMaterializer

import akka.actor.ActorSystem

import services.JobService

class Module extends AbstractModule {

  def configure() = {
    bind(classOf[JobService]).asEagerSingleton()

  }

}
