package controllers

import javax.inject._
import play.api._
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

import akka.actor._

import actors.PageActor

import websockets.WebSocketUtil

import play.api.libs.json._

import play.api.Logger

import play.api.libs.ws._

@Singleton
class WebSocketController @Inject() (wsClient: WSClient) (implicit ec: ExecutionContext) extends Controller {

  def ws(sid: String) = WebSocketUtil.get[JsValue] { (out: ActorRef) ⇒

    // _root_.util.AkkaSupport.system.scheduler.scheduleOnce(3 seconds) {
    //   wsClient.url("http://localhost/amadou/jobs/1000").post("")
    // }

    Props(new PageActor(sid, out))
  }

}

