package websockets

import akka.actor._
import akka.util._

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

  import play.api.mvc.WebSocket

import play.api.mvc.WebSocket.FrameFormatter

import play.api.Play.current

import scala.reflect.ClassTag

object WebSocketUtil {

  def get[T: ClassTag](
    props: (ActorRef) ⇒ Props
  )(implicit fm: FrameFormatter[T], ec: ExecutionContext): WebSocket[T, T] = {
    WebSocket.tryAcceptWithActor[T, T] { implicit request ⇒
      Future.successful(Right((out: ActorRef) ⇒ props(out)))

    }
  }
}
